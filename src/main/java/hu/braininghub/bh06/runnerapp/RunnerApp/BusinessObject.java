package hu.braininghub.bh06.runnerapp.RunnerApp;

import java.util.UUID;

public class BusinessObject {

	private String id;
	private Boolean isactive;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Boolean getIsactive() {
		return isactive;
	}
	public void setIsactive(Boolean isactive) {
		this.isactive = isactive;
	}
	
	public BusinessObject(Boolean isactive) {
		super();
		
		this.id = UUID.randomUUID().toString();
		this.isactive = isactive;
	}
	
	
}
