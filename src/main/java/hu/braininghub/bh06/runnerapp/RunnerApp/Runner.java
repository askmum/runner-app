package hu.braininghub.bh06.runnerapp.RunnerApp;
import java.sql.Date;

public class Runner extends BusinessObject{

	private String firstname;
	private String lastname;
	private Date birthdate;
	private Gender gender;
	private Vehicle vehicle;
	
	public Runner(Boolean isactive, String firstname, String lastname, Date birthdate, Gender gender,
			Vehicle vehicle) {
		super(isactive);
		this.firstname = firstname;
		this.lastname = lastname;
		this.birthdate = birthdate;
		this.gender = gender;
		this.vehicle = vehicle;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public Date getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(Date birthdate) {
		this.birthdate = birthdate;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public Vehicle getVehicle() {
		return vehicle;
	}

	public void setVehicle(Vehicle vehicle) {
		this.vehicle = vehicle;
	}
	
	
}
