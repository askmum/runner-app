package hu.braininghub.bh06.runnerapp.RunnerApp;

public enum Gender {

	MALE, FEMALE, OTHER;
}
